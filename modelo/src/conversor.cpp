#include "conversor.h"

float celsius2Fahrenheit (float temp_C) {
    float  temp_F = (temp_C * 1.8 + 32); // calculo para conversao de celsius para Fahrenheit
    return temp_F;
}

float fahrenheit2Celsius (float temp_F) {
    float temp_C = ((temp_F - 32)/1.8); // calculo para conversao de Fahrenheit para celsius
    return temp_C;
}
