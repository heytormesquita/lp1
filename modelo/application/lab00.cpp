#include "conversor.h"
#include <iostream>

using namespace std;

int main(int argc, char * argv[])
{
    int op; // Opcao a ser digitada pelo usuario
    float temp; // Temperatura a ser convertida
    float tConverted; // Temperatura convertida


    cout << "Conversor de temperatura Celsius <-> Fahrenheit" << endl;
    cout << "(1) Converter de graus Celsius para graus Fahrenheit" << endl;
    cout << "(2) Converter de graus Fahrenheit para graus Celsius" << endl;
    cout << "Digite sua opcao: ";
    cin >> op;

    while(op != 1 and op != 2)
    {
      cout << "Valor invalido, digite um valor valido!" << endl;
      cout << "(1) Converter de graus Celsius para graus Fahrenheit" << endl;
      cout << "(2) Converter de graus Fahrenheit para graus Celsius" << endl;
      cout << "Digite sua opcao: ";
      cin >> op; // leitura da opcao
    }

    if(op == 1){
      cout << "Digite a temperatura em graus Celsius: " << endl;
      cin >> temp; // leitura do valor a ser convertido
      tConverted = celsius2Fahrenheit(temp); // chamada da funcao para conversao do valor lido
      cout << temp << " graus Celsius equivale a " << tConverted << " graus Fahrenheit."<< endl;
    } else {
      cout << "Digite a temperatura em graus Fahrenheit: " << endl;
      cin >> temp; // leitura do valor a ser convertido
      tConverted = fahrenheit2Celsius(temp); // chamada da funcao para conversao do valor lido
      cout << temp << " graus Fahrenheit equivale a " << tConverted << " graus Celsius."<< endl;
    }




    return 0;
}
